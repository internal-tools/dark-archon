from xmlrpc.client import ServerProxy

from fastapi import APIRouter, HTTPException

router = APIRouter()
# TODO: How to communicate with multiple supervisor instances
server = ServerProxy('http://localhost:9001/RPC2')


@router.get('/api/v1/supervisor/control/{command}')
async def control(command: str):
    """ Management commands for supervisor itself

    :param command: supervisor command
    :return: command result
    """
    if command == 'status':
        return server.supervisor.getState()
    elif command == 'pid':
        return server.supervisor.getPID()
    elif command == 'shutdown':
        return server.supervisor.shutdown()
    elif command == 'restart':
        return server.supervisor.restart()
    elif command == 'reload':
        return server.supervisor.reloadConfig()
    else:
        raise HTTPException(status_code=404, detail='Command not found')


@router.get('/api/v1/supervisor/process/{command}/{name}')
async def control_process(command: str, name: str):
    """ Control single process

    :param command: process management command
    :param name: process with group name
    :return: command result
    """
    all_processes = server.supervisor.getAllProcessInfo()
    try:
        searched_process = list(filter(lambda process: process['name'] == name, all_processes))[0]
        process_name = f'{searched_process["group"]}:{searched_process["name"]}'
        if command == 'info':
            return searched_process
        elif command == 'start':
            return server.supervisor.startProcess(process_name)
        elif command == 'stop':
            return server.supervisor.stopProcess(process_name)
        else:
            raise HTTPException(status_code=404, detail='Command not found')
    except IndexError:
        raise HTTPException(status_code=404, detail='Process not found')


@router.get('/api/v1/supervisor/process/group/{command}/{name}')
async def control_process_group(command: str, name: str):
    """ Control group processes

    :param command: process management command
    :param name: process group name
    :return: command result
    """
    if command == 'info':
        all_processes = server.supervisor.getAllProcessInfo()
        return list(filter(lambda process: process['group'] == name, all_processes))
    elif command == 'start':
        return server.supervisor.startProcessGroup(name)
    elif command == 'stop':
        return server.supervisor.stopProcessGroup(name)
    else:
        raise HTTPException(status_code=404, detail='Command not found')


@router.get('/api/v1/supervisor/processes/{command}')
async def control_processes(command: str):
    """ Control all processes at once

    :param command: all process management command
    :return: command result
    """
    if command == 'info':
        return server.supervisor.getAllProcessInfo()
    elif command == 'start':
        return server.supervisor.startAllProcesses()
    elif command == 'stop':
        return server.supervisor.stopAllProcesses()
    else:
        raise HTTPException(status_code=404, detail='Command not found')
