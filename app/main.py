from fastapi import FastAPI

from control.views import router as supervisor_router

app = FastAPI()

app.include_router(supervisor_router)
